package no.ntnu.eliaseb;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.ArrayList;

import static javafx.application.Application.launch;

public class CardGameClient extends Application {


    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("JavaFX poker application");

        DeckOfCards deck = new DeckOfCards();
        HandOfCards hand = new HandOfCards(5, deck);

        HBox cardsBox = new HBox();
        cardsBox.getChildren().setAll(updateHand(hand));

        HBox buttonBox = new HBox();
        Button dealHand = new Button("Deal Hand");
        dealHand.setOnAction(e -> {hand.dealHandOfCards(5);
            cardsBox.getChildren().setAll(updateHand(hand));
        });

        Button checkHand = new Button("Check Hand");
        buttonBox.getChildren().addAll(checkHand, dealHand);



        Text sum = new Text("Sum of faces: " + hand.sumOfFaces());
        Text queenOfSpades = new Text("Queen of spades: " + hand.hasQueenOfSpades());
        Text flush = new Text("Flush:  " + hand.flush());

        HBox infoBox = new HBox();
        infoBox.setSpacing(40);
        infoBox.getChildren().addAll(sum, queenOfSpades, flush);

        HBox heartsBox = new HBox();
        heartsBox.getChildren().setAll(updateHearts(hand));

        checkHand.setOnAction(e -> {
            sum.setText("Sum of faces: " + hand.sumOfFaces());
            queenOfSpades.setText("Queen of spades: " + hand.hasQueenOfSpades());
            flush.setText("Flush:  " + hand.flush());
            heartsBox.getChildren().setAll(updateHearts(hand));
        });

        VBox outerVBox = new VBox();
        outerVBox.getChildren().addAll(cardsBox, buttonBox, infoBox, heartsBox);

        Scene scene = new Scene(outerVBox, 600, 600);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    private ArrayList<Text> updateHand(HandOfCards hand){
        ArrayList<Text> cardTexts = new ArrayList<>();
        for (PlayingCard card :hand.getCards()) {
            cardTexts.add(new Text(card.getASCIIcard()));
        }
        return cardTexts;
    }

    private ArrayList<Text> updateHearts(HandOfCards hand){
        ArrayList<Text> cardTexts = new ArrayList<>();
        cardTexts.add(new Text("Cards of hearts:  "));
        for (PlayingCard card :hand.cardsOfHearts()){
            cardTexts.add(new Text(card.getASCIIcard()));
        }
        if(hand.cardsOfHearts().isEmpty()){
            cardTexts.remove(0);
            cardTexts.add(new Text("No cards of hears"));
        }
        return cardTexts;
    }
}
