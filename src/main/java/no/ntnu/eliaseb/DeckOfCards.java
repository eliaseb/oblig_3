package no.ntnu.eliaseb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

/**
 * This class represent a deck of playing cards.
 * It's only field is an arraylist of PlayingCards representing the deck of cards.
 */
public class DeckOfCards {
    private ArrayList<PlayingCard> cards;

    /**
     * This method shuffles the deck of cards.
     */
    public void shuffle(){
        Collections.shuffle(cards);
    }

    /**
     * This method creates an instance of DeckOfCards, then shuffles them.
     */
    public DeckOfCards() {
        cards = new ArrayList<>();
        char[] suiteList = { 'S', 'H', 'D', 'C' };
        for (char suite: suiteList) {
            for (int face = 1; face < 14; face++) {
                cards.add(new PlayingCard(suite, face));
            }
        }
    }

    /**
     * This method removes n cards from the deck and returns them in an arraylist.
     * @param n the number of cards we wish to remove.
     * @return The arraylist of cards removed.
     */
    public ArrayList<PlayingCard> dealCards(int n){
        ArrayList<PlayingCard> dealtCards = new ArrayList<>();
        if (n>0 && n<cards.size()){
            shuffle();
            for (int i = 0; i < n; i++) {
                dealtCards.add(cards.get(i));
            }
        }
        return dealtCards;
    }

    /**
     * Get's the arraylist of playing cards.
     * @return the arraylist of playing cards.
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }

    /**
     * Sets a new arraylist of playing cards.
     * @param cards The new arraylist of playing cards.
     */
    public void setCards(ArrayList<PlayingCard> cards) {
        this.cards = cards;
    }

    /**
     * Indicates whether some object o equals the Deck of cards.
     * @param o the object we are checking against.
     * @return true if they are the same, false if they are not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeckOfCards that = (DeckOfCards) o;
        return Objects.equals(cards, that.cards);
    }

    /**
     * Returns a hashcode value for the deck of cards
     * @return the hashcode value for the deck of cards.
     */
    @Override
    public int hashCode() {
        return Objects.hash(cards);
    }

    /**
     * Returns a string representation of the deck of cards.
     * @return the string representation of the deck of cards.
     */
    @Override
    public String toString() {
        return "DeckOfCards{" +
                "cards=" + cards +
                '}';
    }
}
