package no.ntnu.eliaseb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * This class represents a hand of cards.
 * It has fields for the arraylist of cards, hand for the deck it draws from.
 */
public class HandOfCards {
    private ArrayList<PlayingCard> cards;
    private DeckOfCards deck;

    /**
     * This method creates an instance of DeckOfCards, then shuffles them.
     */
    public HandOfCards(int n, DeckOfCards deck) {
        this.deck = deck;
        cards = deck.dealCards(n);
    }

    /**
     * This method deals a hand of n cards.
     * @param n the number of cards that are to be dealt.
     */
    public void dealHandOfCards(int n) {
        cards = deck.dealCards(n);
    }


    /**
     * this method returns the sum of all the cards in the hand.
     * @return the sum of the cards in the hand.
     */
    public int sumOfFaces(){
        return cards.stream().map(c -> c.getFace()).reduce(0, (x, y) -> x+y);
    }

    /**
     * This method returns only the cards that are of the suit hearts.
     * @return the arraylist of cards of hearts.
     */
    public ArrayList<PlayingCard> cardsOfHearts(){
        return cards.stream()
                    .filter(c -> c.getSuit() == 'H')
                    .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * This method checks if all the cards are of the same suite.
     * @return true if they are, false if they are not.
     */
    public boolean flush(){
        return (cards.stream().map(c -> c.getSuit()).distinct().count() == 1);
    }

    /**
     * This method checks if the hand contains the queen of spades.
     * @return true if the hand contains the queen of spades, false if it does not.
     */
    public boolean hasQueenOfSpades(){
        return cards.stream().anyMatch(n -> n.getFace()==12 && n.getSuit()=='S');
    }



    /**
     * Get's the arraylist of playing cards.
     * @return the arraylist of playing cards.
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }

    /**
     * Sets a new arraylist of playing cards.
     * @param cards The new arraylist of playing cards.
     */
    public void setCards(ArrayList<PlayingCard> cards) {
        this.cards = cards;
    }

    /**
     * Indicates whether some object o equals the Hand of cards..
     * @param o the object we are checking against.
     * @return true if they are the same, false if they are not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HandOfCards that = (HandOfCards) o;
        return Objects.equals(cards, that.cards) &&
                Objects.equals(deck, that.deck);
    }

    /**
     * Returns a hashcode value for the hand of cards
     * @return the hashcode value for the hand of cards.
     */
    @Override
    public int hashCode() {
        return Objects.hash(cards, deck);

    }


    /**
     * Returns a string representation of the hand of cards.
     * @return the string representation of the hand of cards.
     */
    @Override
    public String toString() {
        return "HandOfCards{" +
                "cards=" + cards +
                ", deck=" + deck +
                '}';
    }
}
