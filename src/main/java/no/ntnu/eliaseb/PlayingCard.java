package no.ntnu.eliaseb;

import java.util.Objects;

/**
 * This class represents a single playing card.
 * It has a char field for the suite of the card and a int field for the face of the card.
 */
public class PlayingCard {
    private final char suit; // 'S'=spade, 'H'=heart, 'D'=diamonds, 'C'=clubs
    private final int face; // a number between 1 and 13

    /**
     * Creates an instance of a PlayingCard with a given suit and face.
     *
     * @param suit The suit of the card, as a single character. 'S' for Spades,
     *             'H' for Heart, 'D' for Diamonds and 'C' for clubs
     * @param face The face value of the card, an integer between 1 and 13
     */
    public PlayingCard(char suit, int face) {
        this.suit = suit;
        this.face = face;
    }

    /**
     * Returns the suit and face of the card as a string.
     * A 4 of hearts is returned as the string "H4".
     *
     * @return the suit and face of the card as a string
     */
    public String getAsString() {
        String spacing = " ";
        if (face<10){
            spacing = "  ";
        }


        return String.format("%s%s", suit, spacing + face);
    }

    /**
     * Returns a textString ASCII representation of the card.
     * @return the ASCII image of the card.
     */
    public String getASCIIcard(){
        String startString = "+-------+  \n" +
                " |            |\n" +
                " |            |\n" +
                " |  ";
        String endString = "   |\n" +
                " |            |\n" +
                " |            |\n" +
                "+-------+";
        return startString + getAsString() + endString;
    }

    /**
     * Returns the suit of the card, 'S' for Spades, 'H' for Heart, 'D' for Diamonds and 'C' for clubs
     *
     * @return the suit of the card
     */
    public char getSuit() {
        return suit;
    }

    /**
     * Returns the face of the card (value between 1 and 13).
     *
     * @return the face of the card
     */
    public int getFace() {
        return face;
    }

    /**
     * Indicates whether som other object o is equal to this card
     * @param o the object we are checking
     * @return true if they are equal, false if they are not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayingCard that = (PlayingCard) o;
        return suit == that.suit &&
                face == that.face;
    }

    /**
     * This method generates the hashcode for the Card
     * @return The hashcode for the card.
     */
    @Override
    public int hashCode() {
        return Objects.hash(suit, face);
    }

    /**
     * Returns a string representation of the object.
     * @return The string representation of the object.
     */
    @Override
    public String toString() {
        return "PlayingCard{" +
                "suit=" + suit +
                ", face=" + face +
                '}';
    }
}

