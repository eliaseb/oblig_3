package no.ntnu.eliaseb;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlayingCardTest {
    @Test
    public void playingCardReturnsSuite(){
        PlayingCard card = new PlayingCard('S', 8);
        assertEquals('S', card.getSuit());

    }

    @Test
    public void playingCardReturnsFace(){
        PlayingCard card = new PlayingCard('S', 8);
        assertEquals(8, card.getFace());

    }
}
